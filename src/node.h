#ifndef NODE_H
#define NODE_H

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

#include "mesh.h"

typedef struct node {
    mesh_t mesh;
    char is_active;
    char mesh_copy;
    char texture_copy;
    float x_pos;
    float y_pos;
    float z_pos;
    float rotation_horizontal;
    float rotation_vertical;
    float x_scale;
    float y_scale;
    float z_scale;
} node_t;

extern void node_init(node_t *this);
extern void node_free(node_t *this);
extern char node_load_mesh_obj(node_t *this, const char *path);
extern char node_copy_mesh_obj(node_t *this, node_t *parent);
extern void node_set_pos(node_t *this, float x, float y, float z);
extern void node_set_rot(node_t *this, float rotation_horizontal, float rotation_vertical);
extern void node_set_scale(node_t *this, float x, float y, float z);
extern void node_set_visibility(node_t *this, char is_visible);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif  /* NODE_H */
