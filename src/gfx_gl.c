#include <stddef.h>

#define GL_GLEXT_PROTOTYPES
#ifndef __APPLE__
#include <GL/gl.h>
#include <GL/glext.h>
#else
#define GL_SILENCE_DEPRECATION
#include <OpenGL/gl.h>
#include <OpenGL/glext.h>
#endif /* __APPLE__ */

#include "util.h"
#include "system.h"
#include "gfx.h"

#define _GFX_TEXTURE_MAX        256
#define _GFX_DRAW_ENTRY_MAX     256

#define _VERTEX_INDEX   0
#define _COLOR_INDEX    1
#define _NORMALS_INDEX  2
#define _TEXCOORD_INDEX 3
#define _INDEX_INDEX    4
#define _BUFFER_COUNT   5

typedef struct gfx_texture {
    unsigned int id;
    char is_active;
    GLuint handle;
    char *name;
} gfx_texture_t;

typedef struct gfx_draw_entry {
    unsigned int id;
    char is_active;
    char is_visible;
    unsigned int indices_count;
    GLuint buffers[_BUFFER_COUNT];
    int texture_id;
    float x_pos;
    float y_pos;
    float z_pos;
    float rotation_horizontal;
    float rotation_vertical;
    float x_rot;
    float y_rot;
    float z_rot;
    float x_scale;
    float y_scale;
    float z_scale;
} gfx_draw_entry_t;

static char _is_perspective;
static float _scale_factor;

static gfx_texture_t _gfx_textures[_GFX_TEXTURE_MAX];
static unsigned int _gfx_texture_count;

static void _gfx_texture_free(gfx_texture_t *texture);

static gfx_draw_entry_t _gfx_draw_entries[_GFX_DRAW_ENTRY_MAX];
static unsigned int _gfx_entry_draw_count;

static int _gfx_entry_next_index(void);
static gfx_draw_entry_t *_gfx_get_entry(int *index);
static void _gfx_entry_init(gfx_draw_entry_t *this);
static void _gfx_entry_draw(const gfx_draw_entry_t *entry);
static void _gfx_entry_free(gfx_draw_entry_t *entry);

static GLfloat _camera_pos_x;
static GLfloat _camera_pos_y;
static GLfloat _camera_pos_z;

static GLfloat _camera_rotation_horizontal;
static GLfloat _camera_rotation_vertical;
static GLfloat _camera_x_rot;
static GLfloat _camera_y_rot;
static GLfloat _camera_z_rot;

static GLuint _program;

static char _gfx_program_init(void);
static void _gfx_program_free(void);

char gfx_init(void)
{
    const char is_perspective = 1;
    const float scale_factor = 50.0f;

    GLfloat spec[] = {
        1.0, 1.0, 1.0, 1.0
    };
    GLfloat shiny[] = { 50.0 };
    GLfloat light_pos[] = {
        0.0, 0.0, 1.0, 0.0
    };

    _is_perspective = is_perspective;
    _scale_factor = scale_factor;
    if (_gfx_program_init() != 0)
        return 1;
    _gfx_entry_draw_count = 0;

    _camera_pos_x = 0.0f;
    _camera_pos_y = 0.0f;
    _camera_pos_z = 0.0f;

    _camera_rotation_horizontal = 0.0f;
    _camera_rotation_vertical = 0.0f;
    _camera_x_rot = 0.0f;
    _camera_y_rot = 1.0f;
    _camera_z_rot = 0.0f;

    glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

    glEnable(GL_DEPTH_TEST);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glMaterialfv(GL_FRONT, GL_SPECULAR, spec);
    glMaterialfv(GL_FRONT, GL_SHININESS, shiny);
    glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
    glShadeModel(GL_SMOOTH);

    glEnable(GL_TEXTURE_2D);

    gfx_resize();

    return 0;
}

void gfx_free(void)
{
    int i;

    for (i = 0; i < _gfx_entry_draw_count; i++)
        _gfx_entry_free(&_gfx_draw_entries[i]);

    for (i = 0; i < _gfx_texture_count; i++)
        _gfx_texture_free(&_gfx_textures[i]);

    _gfx_program_free();
}

int gfx_texture_create(gfx_texture_create_t *info)
{
    int i;
    GLuint handle;
    gfx_texture_t *texture = 0;

    glGenTextures(1, &handle);
    glBindTexture(GL_TEXTURE_2D, handle);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, info->width, info->height, 0, GL_RGB, GL_FLOAT, info->buffer);
    glGenerateMipmap(GL_TEXTURE_2D);

    for (i = 0; i < _gfx_texture_count; i++) {
        texture = &_gfx_textures[i];

        if (!texture->is_active || !texture->name)
            break;
    }

    texture = &_gfx_textures[i];

    _gfx_texture_count++;
    texture->id = i;
    texture->is_active = 1;
    texture->handle = handle;
    texture->name = util_strdup(info->name);

    return texture->id;
}

char gfx_is_texture_in_cache(const char *name)
{
    int i;
    char is_match = 0;

    for (i = 0; i < _gfx_texture_count; i++) {
        gfx_texture_t *texture = &_gfx_textures[i];

        if (!texture->is_active)
            continue;

        if (!texture->name)
            continue;

        is_match = !util_strcmp(name, texture->name);

        if (is_match)
            break;
    }

    return is_match;
}

void gfx_texture_free(const char *name)
{
    int i;
    char is_match = 0;

    for (i = 0; i < _gfx_texture_count; i++) {
        gfx_texture_t *texture = &_gfx_textures[i];

        if (!texture->is_active)
            continue;

        if (!texture->name)
            continue;

        is_match = !util_strcmp(name, texture->name);

        if (is_match) {
            _gfx_texture_free(texture);
            break;
        }
    }
}

void gfx_draw(void)
{
    int i;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glRotatef(_camera_rotation_horizontal, 0.0f, 1.0f, 0.0f);
    glRotatef(_camera_rotation_vertical, _camera_x_rot, _camera_y_rot, _camera_z_rot);
    glTranslatef(-_camera_pos_x, -_camera_pos_y, -_camera_pos_z);

    for (i = 0; i < _gfx_entry_draw_count; i++) {
        gfx_draw_entry_t *entry = &_gfx_draw_entries[i];

        if (entry->is_visible) {
            glPushMatrix();
                glTranslatef(entry->x_pos, entry->y_pos, entry->z_pos);
                glRotatef(entry->rotation_horizontal, 0.0f, 1.0f, 0.0f);
                glRotatef(entry->rotation_vertical, entry->x_rot, entry->y_rot, entry->z_rot);
                glScalef(entry->x_scale, entry->y_scale, entry->z_scale);
                _gfx_entry_draw(entry);
            glPopMatrix();
        }
    }
}

int gfx_entry_create(gfx_entry_create_t *info)
{
    int entry_index;
    gfx_draw_entry_t *entry;

    entry = _gfx_get_entry(&entry_index);

    if (entry_index == -1)
        return -1;

    _gfx_entry_init(entry);

    entry->id = entry_index;
    entry->indices_count = info->indices_count;

    glGenBuffers(_BUFFER_COUNT, entry->buffers);

    if (info->vertex_count) {
        glBindBuffer(GL_ARRAY_BUFFER, entry->buffers[_VERTEX_INDEX]);
        glBufferData(GL_ARRAY_BUFFER, info->vertex_count * sizeof (*info->vertices), info->vertices, GL_STATIC_DRAW);
    } else {
        glDeleteBuffers(1, &entry->buffers[_VERTEX_INDEX]);
        entry->buffers[_VERTEX_INDEX] = 0;
    }

    if (info->color_count) {
        glBindBuffer(GL_ARRAY_BUFFER, entry->buffers[_COLOR_INDEX]);
        glBufferData(GL_ARRAY_BUFFER, info->color_count * sizeof (*info->colors), info->colors, GL_STATIC_DRAW);
    } else {
        glDeleteBuffers(1, &entry->buffers[_COLOR_INDEX]);
        entry->buffers[_COLOR_INDEX] = 0;
    }

    if (info->normal_count) {
        glBindBuffer(GL_ARRAY_BUFFER, entry->buffers[_NORMALS_INDEX]);
        glBufferData(GL_ARRAY_BUFFER, info->normal_count * sizeof (*info->normals), info->normals, GL_STATIC_DRAW);
    } else {
        glDeleteBuffers(1, &entry->buffers[_NORMALS_INDEX]);
        entry->buffers[_NORMALS_INDEX] = 0;
    }

    if (info->texcoord_count) {
        glBindBuffer(GL_ARRAY_BUFFER, entry->buffers[_TEXCOORD_INDEX]);
        glBufferData(GL_ARRAY_BUFFER, info->texcoord_count * sizeof (*info->texcoords), info->texcoords, GL_STATIC_DRAW);
    } else {
        glDeleteBuffers(1, &entry->buffers[_TEXCOORD_INDEX]);
        entry->buffers[_TEXCOORD_INDEX] = 0;
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, entry->buffers[_INDEX_INDEX]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, info->indices_count * sizeof (*info->indices), info->indices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    return entry->id;
}

int gfx_entry_dup(unsigned int entry_id)
{
    int entry_index;
    gfx_draw_entry_t *entry;

    entry = _gfx_get_entry(&entry_index);
    if (entry_index == -1)
        return -1;

    util_memcpy(entry, &_gfx_draw_entries[entry_id], sizeof (gfx_draw_entry_t));
    entry->id = entry_index;

    return entry->id;
}

void gfx_entry_position(unsigned int id, float x, float y, float z)
{
    _gfx_draw_entries[id].x_pos = x;
    _gfx_draw_entries[id].y_pos = y;
    _gfx_draw_entries[id].z_pos = z;
}

void gfx_entry_rotation(unsigned int id, float rotation_horizontal, float rotation_vertical)
{
    _gfx_draw_entries[id].rotation_horizontal = rotation_horizontal;
    _gfx_draw_entries[id].rotation_vertical = rotation_vertical;

    _gfx_draw_entries[id].x_rot = 0.0f;
    _gfx_draw_entries[id].y_rot = 1.0f;
    _gfx_draw_entries[id].z_rot = 0.0f;
}

void gfx_entry_scale(unsigned int id, float x, float y, float z)
{
    _gfx_draw_entries[id].x_scale = x;
    _gfx_draw_entries[id].y_scale = y;
    _gfx_draw_entries[id].z_scale = z;
}

void gfx_entry_set_texture(unsigned int id, int texture_id)
{
    _gfx_draw_entries[id].texture_id = texture_id;
}

void gfx_entry_set_visibility(unsigned int id, char is_visible)
{
    _gfx_draw_entries[id].is_visible = is_visible;
}

void gfx_resize(void)
{
    unsigned int width, height;
    float aspect_ratio, scale_ratio;

    system_display_props(&width, &height);
    aspect_ratio = (float) width / (float) height;
    scale_ratio = aspect_ratio * _scale_factor;

    glViewport(0, 0, width, height);    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if (_is_perspective)
        glFrustum(-scale_ratio, scale_ratio, -_scale_factor, _scale_factor, _scale_factor, _scale_factor * 10.0f);
    else
        glOrtho(-scale_ratio, scale_ratio, -_scale_factor, _scale_factor, -_scale_factor, _scale_factor);
}

void gfx_set_camera_position(float camera_pos_x, float camera_pos_y, float camera_pos_z)
{
    _camera_pos_x = camera_pos_x;
    _camera_pos_y = camera_pos_y;
    _camera_pos_z = camera_pos_z;
}

void gfx_set_camera_rotation(float camera_rotation_horizontal, float camera_rotation_vertical)
{
    _camera_rotation_horizontal = camera_rotation_horizontal;
    _camera_rotation_vertical = camera_rotation_vertical;
}

void gfx_set_camera_vert_axis(float x, float y, float z)
{
    _camera_x_rot = x;
    _camera_y_rot = y;
    _camera_z_rot = z;
}

static void _gfx_texture_free(gfx_texture_t *texture)
{
    if (texture->is_active) {
        texture->is_active = 0;

        glDeleteTextures(1, &texture->handle);
        texture->handle = 0;
        util_strdup_free(texture->name);
        texture->name = 0;
    }
}

static int _gfx_entry_next_index(void)
{
    if (_gfx_entry_draw_count + 1 > _GFX_DRAW_ENTRY_MAX)
        return -1; 

    return _gfx_entry_draw_count++;
}

static gfx_draw_entry_t *_gfx_get_entry(int *index)
{
    if ((*index = _gfx_entry_next_index()) == -1)
        return 0;
    else
        return &_gfx_draw_entries[*index];
}

static void _gfx_entry_init(gfx_draw_entry_t *this)
{
    this->is_active = 1;
    this->is_visible = 1;
    this->x_pos = 0.0f;
    this->y_pos = 0.0f;
    this->z_pos = 0.0f;
    this->rotation_horizontal = 0.0f;
    this->rotation_vertical = 0.0f;
    this->x_rot = 0.0f;
    this->y_rot = 1.0f;
    this->z_rot = 0.0f;
    this->x_scale = 1.0f;
    this->y_scale = 1.0f;
    this->z_scale = 1.0f;
}

static void _gfx_entry_draw(const gfx_draw_entry_t *entry)
{
    if (entry->buffers[_VERTEX_INDEX])
        glEnableVertexAttribArray(0);
    if (entry->buffers[_COLOR_INDEX])
        glEnableClientState(GL_COLOR_ARRAY);
    if (entry->buffers[_NORMALS_INDEX])
        glEnableClientState(GL_NORMAL_ARRAY);
    if (entry->buffers[_TEXCOORD_INDEX])
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    if (entry->buffers[_VERTEX_INDEX]) {
        glBindBuffer(GL_ARRAY_BUFFER, entry->buffers[_VERTEX_INDEX]);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    }

    if (entry->buffers[_COLOR_INDEX]) {
        glBindBuffer(GL_ARRAY_BUFFER, entry->buffers[_COLOR_INDEX]);
        glColorPointer(3, GL_FLOAT, 0, 0);
    }

    if (entry->buffers[_NORMALS_INDEX]) {
        glBindBuffer(GL_ARRAY_BUFFER, entry->buffers[_NORMALS_INDEX]);
        glNormalPointer(GL_FLOAT, 0, NULL);
    }

    if (entry->buffers[_TEXCOORD_INDEX]) {
        glBindBuffer(GL_ARRAY_BUFFER, entry->buffers[_TEXCOORD_INDEX]);
        glTexCoordPointer(2, GL_FLOAT, 0, NULL);
    }

    if (entry->texture_id != -1) {
        glBindTexture(GL_TEXTURE_2D, _gfx_textures[entry->texture_id].handle);
        glUniform1i(glGetUniformLocation(_program, "tex"), 0);
    }

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, entry->buffers[_INDEX_INDEX]);
    glDrawElements(GL_TRIANGLES, entry->indices_count, GL_UNSIGNED_INT, 0);

    if (entry->buffers[_VERTEX_INDEX])
        glDisableVertexAttribArray(0);
    if (entry->buffers[_COLOR_INDEX])
        glDisableClientState(GL_COLOR_ARRAY);
    if (entry->buffers[_NORMALS_INDEX])
        glDisableClientState(GL_NORMAL_ARRAY);
    if (entry->buffers[_TEXCOORD_INDEX])
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

static void _gfx_entry_free(gfx_draw_entry_t *entry)
{
    glDeleteBuffers(_BUFFER_COUNT, entry->buffers);
}

static const char *_vertex_shader_source[] = {
    "varying vec3 lightDir,normal;",
    "",
    "void main()",
    "{",
    "   float NdotL;",
    "   vec4 diffuse;",
    "",
    "   gl_Position = ftransform();",
    "",
    "   lightDir = normalize(vec3(gl_LightSource[0].position));",
    "   NdotL = max(dot(normal, lightDir), 0.0);",
    "   diffuse = gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse;",
    "   gl_FrontColor = NdotL * diffuse;",
    "",
    "   normal = normalize(gl_NormalMatrix * gl_Normal);",
    "",
    "   gl_TexCoord[0] = gl_MultiTexCoord0;",
    "",
    "}"
};

static const char *_fragment_shader_source[] = {
    "varying vec3 lightDir,normal;",
    "uniform samplerBuffer tex;",
    "",
    "void main()",
    "{",
    "   vec4 texel;",
    "   vec3 c_frag, c_texel;",
    "   float a_frag, a_texel;",
    "   float intensity;",
    "",
    "   intensity = max(dot(lightDir, normalize(normal)), 0.0);",
    "   c_frag = intensity * (gl_FrontMaterial.diffuse).rgb + gl_FrontMaterial.ambient.rgb;",
    "   a_frag = gl_FrontMaterial.diffuse.a;",
    "",
    "   texel = texture2D(tex, gl_TexCoord[0].st);",
    "   c_texel = texel.rgb;",
    "   a_texel = texel.a;",
    "",
    "   gl_FragColor = vec4(c_frag * c_texel, a_frag * a_texel);",
    "}"
};

static char _gfx_program_init(void)
{
    GLuint vertex_shader, fragment_shader;
    int compile_status;

    vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(
        vertex_shader,
        sizeof (_vertex_shader_source) / sizeof (char *),
        _vertex_shader_source,
        NULL
    );
    glCompileShader(vertex_shader);

    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(
        fragment_shader,
        sizeof (_fragment_shader_source) / sizeof (char *),
        _fragment_shader_source,
        NULL
    );
    glCompileShader(fragment_shader);

    _program = glCreateProgram();
    glAttachShader(_program, vertex_shader);
    glAttachShader(_program, fragment_shader);
    glLinkProgram(_program);
    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);
    glUseProgram(_program);

    return 0;
}

static void _gfx_program_free(void)
{
    glUseProgram(0);
    glDeleteProgram(_program);
}
