#include <stddef.h>

#include <X11/Xlib.h>
#include <X11/XKBlib.h>

#include <GL/glx.h>

#include "system.h"

#define DEFAULT_WIDTH   640
#define DEFAULT_HEIGHT  480
#define EVENT_MASK      KeyPressMask | KeyReleaseMask

static char _running;
static char _fullscreen;
static system_key_t _key_pressed;
static system_key_t _key_held;
static unsigned int _display_width;
static unsigned int _display_height;
static const char *_name;

static Display *_display;
static Window _w;
static Cursor _empty_cursor;

static int _error_base;
static int _event_base;
static GLXFBConfig _config;
static GLXWindow _win;
static GLXContext _ctx;

static void _system_key_press(XKeyEvent *event);
static void _system_key_release(XKeyEvent *event);
static system_key_t _system_keysym_to_key(KeySym keysym);

char system_init(void)
{
    const char fullscreen = 0;
    const unsigned int width = DEFAULT_WIDTH, height = DEFAULT_HEIGHT;
    const char *name = "OpenGL";

    int i;

    int default_screen;
    int x, y;
    XVisualInfo *visual_info;
    unsigned long valuemask; 
    XSetWindowAttributes attributes;
    Pixmap null_pixmap;
    XColor null_color = { 0 };

    const int attrib_list[] = {
        GLX_DOUBLEBUFFER,   True,
        GLX_RED_SIZE,       8,
        GLX_GREEN_SIZE,     8,
        GLX_BLUE_SIZE,      8,
        GLX_ALPHA_SIZE,     8,
        GLX_DEPTH_SIZE,     24,
        GLX_STENCIL_SIZE,   8,
        GLX_RENDER_TYPE,    GLX_RGBA_BIT,
        GLX_DRAWABLE_TYPE,  GLX_WINDOW_BIT,
        GLX_X_RENDERABLE,   True,
        None
    };
    int major, minor;
    GLXFBConfig *configs;
    int config_nelements;

    _running = 1;
    _fullscreen = fullscreen;
    _key_pressed = 0;
    _key_held = 0;
    _name = name;

    if ((_display = XOpenDisplay(NULL)) == NULL)
        return 1;
    default_screen = XDefaultScreen(_display);
    XkbSetDetectableAutoRepeat(_display, True, NULL);

    if (glXQueryExtension(_display, &_error_base, &_event_base) == False)
        return 1;
    if (glXQueryVersion(_display, &major, &minor) == False)
        return 1;
    if (!(major == 1 && minor == 4))
        return 1;

    configs = glXChooseFBConfig(_display, default_screen, attrib_list, &config_nelements);
    for (i = 0; i < config_nelements; i++)
        if (i == 0) {
            _config = configs[i];
            break;
        }
    XFree(configs);

    visual_info = glXGetVisualFromFBConfig(_display, _config);
    valuemask = CWEventMask;
    attributes.event_mask = EVENT_MASK;
    if (_fullscreen) {
        _display_width = XDisplayWidth(_display, default_screen);
        _display_height = XDisplayHeight(_display, default_screen);
        valuemask |= CWOverrideRedirect;
        attributes.override_redirect = True;
    } else {
        _display_width = width;
        _display_height = height;
    }
    _w = XCreateWindow(
        _display,
        XDefaultRootWindow(_display),
        0,
        0,
        _display_width,
        _display_height,
        0,
        CopyFromParent,
        CopyFromParent,
        visual_info->visual,
        valuemask,
        &attributes
    );
    XFree(visual_info);
    XStoreName(_display, _w, _name);

    null_pixmap = XCreatePixmap(_display, _w, 1, 1, 1);
    _empty_cursor = XCreatePixmapCursor(_display, null_pixmap, null_pixmap, &null_color, &null_color, 0, 0);
    XFreePixmap(_display, null_pixmap);
    if (fullscreen)
        XDefineCursor(_display, _w, _empty_cursor);

    _win = glXCreateWindow(_display, _config, _w, NULL);
    if ((_ctx = glXCreateNewContext(_display, _config, GLX_RGBA_TYPE, NULL, True)) == NULL)
        return 1;
    if (glXMakeContextCurrent(_display, _win, _win, _ctx) == False)
        return 1;

    XMapRaised(_display, _w);
    XGrabKeyboard(_display, _w, True, GrabModeAsync, GrabModeAsync, CurrentTime);

    return 0;
}

void system_step(void)
{
    XEvent event;

    glXSwapBuffers(_display, _win);

    _key_pressed = 0;
    while (XPending(_display)) {
        XNextEvent(_display, &event);

        switch (event.type) {
            case KeyPress:
                _system_key_press(&event.xkey);
                break;
            case KeyRelease:
                _system_key_release(&event.xkey);
                break;
            default:
                break;
        }
    }
}

void system_free(void)
{
    XUngrabKeyboard(_display, CurrentTime);
    XUnmapWindow(_display, _w);

    glXMakeContextCurrent(_display, None, None, NULL);
    glXDestroyContext(_display, _ctx);
    glXDestroyWindow(_display, _win);

    XFreeCursor(_display, _empty_cursor);

    XDestroyWindow(_display, _w);

    XCloseDisplay(_display);
}

char system_running(void)
{
    return _running;
}

void system_display_props(unsigned int *width, unsigned int *height)
{
    *width = _display_width;
    *height = _display_height;
}

void system_get_key_state(system_key_t *pressed, system_key_t *held)
{
    *pressed = _key_pressed;
    *held = _key_held;
}

void system_toggle_fullscreen(void)
{
    XWindowAttributes current_attributes;
    XSetWindowAttributes new_attributes = { 0 };

    XUngrabKeyboard(_display, CurrentTime);
    XUnmapWindow(_display, _w);

    glXMakeContextCurrent(_display, None, None, NULL);
    glXDestroyWindow(_display, _win);

    XGetWindowAttributes(_display, _w, &current_attributes);
    XDestroyWindow(_display, _w);

    new_attributes.event_mask = EVENT_MASK;
    if (_fullscreen) {
        _display_width = DEFAULT_WIDTH;
        _display_height = DEFAULT_HEIGHT;

        new_attributes.override_redirect = False;
        new_attributes.cursor = None;
    } else {
        int default_screen = XDefaultScreen(_display);
        _display_width = XDisplayWidth(_display, default_screen);
        _display_height = XDisplayHeight(_display, default_screen);

        new_attributes.override_redirect = True;
        new_attributes.cursor = _empty_cursor;
    }

    _w = XCreateWindow(
        _display,
        XDefaultRootWindow(_display),
        0,
        0,
        _display_width,
        _display_height,
        0,
        CopyFromParent,
        CopyFromParent,
        current_attributes.visual,
        CWEventMask | CWOverrideRedirect | CWCursor,
        &new_attributes
    );
    XStoreName(_display, _w, _name);
    _win = glXCreateWindow(_display, _config, _w, NULL);
    glXMakeContextCurrent(_display, _win, _win, _ctx);
    XMapRaised(_display, _w);
    XGrabKeyboard(_display, _w, True, GrabModeAsync, GrabModeAsync, CurrentTime);

    _fullscreen = !_fullscreen;
}

void system_quit(void)
{
    _running = 0;
}

static void _system_key_press(XKeyEvent *event)
{
    KeySym keysym;
    system_key_t key = 0;

    XLookupString(event, NULL, 0, &keysym, NULL);
    key = _system_keysym_to_key(keysym);

    if ((_key_pressed & key)) {
        _key_pressed &= ~key;
    } else if (!(_key_held & key)) {
        _key_pressed |= key;
        _key_held |= key;
    }
}

static void _system_key_release(XKeyEvent *event)
{
    KeySym keysym;
    system_key_t key = 0;

    XLookupString(event, NULL, 0, &keysym, NULL);
    key = _system_keysym_to_key(keysym);

    _key_pressed &= ~key;
    _key_held &= ~key;
}

static system_key_t _system_keysym_to_key(KeySym keysym)
{
    switch (keysym) {
        case XK_Escape:
            return SYSTEM_KEY_Esc;
        case XK_A:
        case XK_a:
            return SYSTEM_KEY_A;
        case XK_B:
        case XK_b:
            return SYSTEM_KEY_B;
        case XK_D:
        case XK_d:
            return SYSTEM_KEY_D;
        case XK_F:
        case XK_f:
            return SYSTEM_KEY_F;
        case XK_S:
        case XK_s:
            return SYSTEM_KEY_S;
        case XK_W:
        case XK_w:
            return SYSTEM_KEY_W;
        default:
            return 0;
    }
}
