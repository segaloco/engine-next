#include "util.h"
#include "system.h"
#include "gfx.h"
#include "mesh.h"
#include "node.h"

#include "engine.h"

static const float _movement_factor = 2.0f;

static char _running;

static char _do_fullscreen_toggle;

static node_t camera_node;
static node_t player_node;
static node_t cup_node;
static node_t cup2_node;
static node_t cup3_node;

static node_t *_player_node;
static void _engine_player_move(system_key_t pressed, system_key_t held);

static node_t *_camera_node;
static void _engine_camera_update(void);
static void _engine_camera_follow(node_t *target);

static void _engine_do_routine(void);

static void _engine_do_input(void);

char engine_init(void)
{
    _running = 1;
    _do_fullscreen_toggle = 0;

    node_init(&player_node);
    if (node_load_mesh_obj(&player_node, "assets/cup.obj") != 0)
        return 1;
    node_set_pos(&player_node, 0.0f, 0.0f, -100.0f);
    node_set_scale(&player_node, 0.2f, 0.2f, 0.2f);

    node_init(&cup_node);
    if (node_load_mesh_obj(&cup_node, "assets/cup.obj") != 0)
        return 1;
    node_set_pos(&cup_node, 0.0f, 0.0f, -200.0f);
    node_set_scale(&cup_node, 0.5f, 0.5f, 0.5f);

    node_init(&cup2_node);
    if (node_copy_mesh_obj(&cup2_node, &cup_node) == -1)
        return 1;
    node_set_pos(&cup2_node, 100.0f, 0.0f, -200.0f);
    node_set_scale(&cup_node, 1.0f, 1.0f, 1.0f);

    node_init(&cup3_node);
    if (node_copy_mesh_obj(&cup3_node, &cup_node) == -1)
        return 1;
    node_set_pos(&cup3_node, -100.0f, 0.0f, -200.0f);
    node_set_scale(&cup_node, 1.0f, 1.0f, 1.0f);

    node_init(&camera_node);

    _player_node = &player_node;

    _camera_node = &camera_node;
    _engine_camera_update();

    return 0;
}

void engine_free(void)
{
    node_free(&cup_node);
    node_free(&cup2_node);
    node_free(&cup3_node);
    node_free(&player_node);
    node_free(&camera_node);
}

void engine_step(void)
{
    _engine_do_input();
    _engine_do_routine();

    if (!_running)
        return;

    gfx_draw();
}

static void _engine_do_routine(void)
{
    if (!_running) {
        system_quit();
        return;
    }

    if (_do_fullscreen_toggle) {
        system_toggle_fullscreen();
        gfx_resize();
        _do_fullscreen_toggle = 0;
    }

    if (cup_node.is_active)
        node_set_rot(&cup_node, cup_node.rotation_horizontal + _movement_factor, 0.0f);
    if (cup2_node.is_active)
        node_set_rot(&cup2_node, cup2_node.rotation_horizontal + _movement_factor, 0.0f);
    if (cup3_node.is_active)
        node_set_rot(&cup3_node, cup3_node.rotation_horizontal + _movement_factor, 0.0f);

    _engine_camera_follow(&player_node);
    _engine_camera_update();
}

static void _engine_camera_update(void)
{
    float x_comp, z_comp;

    x_comp = util_sin(_camera_node->rotation_horizontal * (UTIL_PI / 180.0f));
    z_comp = util_cos(_camera_node->rotation_horizontal * (UTIL_PI / 180.0f));

    gfx_set_camera_position(_camera_node->x_pos, _camera_node->y_pos, _camera_node->z_pos);
    gfx_set_camera_rotation(_camera_node->rotation_horizontal, _camera_node->rotation_vertical);
    gfx_set_camera_vert_axis(-z_comp, 0.0f, -x_comp);
}

static void _engine_camera_follow(node_t *target)
{
    const float _camera_dist = 200.0f;

    float angle_h, angle_v;
    float camera_planar_dist;
    float player_planar_dist;
    float _dist_x;
    float _dist_y;
    float _dist_z;

    camera_planar_dist = util_sqrt(util_pow(_camera_node->x_pos, 2) + util_pow(_camera_node->z_pos, 2));
    player_planar_dist = util_sqrt(util_pow(_player_node->x_pos, 2) + util_pow(_player_node->z_pos, 2));

    angle_h = util_lineangle2(_camera_node->x_pos, _camera_node->z_pos, target->x_pos, target->z_pos);
    angle_v = util_lineangle2(camera_planar_dist, _camera_node->y_pos, player_planar_dist, _player_node->y_pos) - 90.0f;

    node_set_rot(_camera_node, angle_h, angle_v);

    _dist_x = util_fabs(_player_node->x_pos - _camera_node->x_pos);
    if (_dist_x >= _camera_dist) {
        _camera_node->x_pos = _player_node->x_pos + _camera_dist;
    }
    _dist_z = util_fabs(_player_node->z_pos - _camera_node->z_pos);
    if (_dist_z >= _camera_dist) {
        _camera_node->z_pos = _player_node->z_pos + _camera_dist;
    }
}

static void _engine_player_move(system_key_t pressed, system_key_t held)
{
    float x_pos, y_pos, z_pos;
    float x_comp, z_comp;

    x_pos = _player_node->x_pos;
    y_pos = _player_node->y_pos;
    z_pos = _player_node->z_pos;
    x_comp = util_sin(_camera_node->rotation_horizontal * (UTIL_PI / 180.0f));
    z_comp = util_cos(_camera_node->rotation_horizontal * (UTIL_PI / 180.0f));

    if ((held & SYSTEM_KEY_W)) {
        z_pos -= _movement_factor * z_comp;
        x_pos += _movement_factor * x_comp;
    }
    if ((held & SYSTEM_KEY_S)) {
        z_pos += _movement_factor * z_comp;
        x_pos -= _movement_factor * x_comp;
    }
    if ((held & SYSTEM_KEY_A)) {
        z_pos -= _movement_factor * x_comp;
        x_pos -= _movement_factor * z_comp;
    }
    if ((held & SYSTEM_KEY_D)) {
        z_pos += _movement_factor * x_comp;
        x_pos += _movement_factor * z_comp;
    }

    if ((held & SYSTEM_KEY_UP)) {
        y_pos += _movement_factor;
    }
    if ((held & SYSTEM_KEY_DOWN)) {
        y_pos -= _movement_factor;
    }

    node_set_pos(_player_node, x_pos, y_pos, z_pos);
}

static void _engine_do_input(void)
{
    float x_pos, z_pos;
    float result;
    float x_comp, z_comp;
    system_key_t pressed, held;
    system_get_key_state(&pressed, &held);

    if ((pressed & (SYSTEM_KEY_Esc | SYSTEM_KEY_Q)))
        _running = 0;

    if ((pressed & SYSTEM_KEY_F))
        _do_fullscreen_toggle = 1;

    _engine_player_move(pressed, held);
}
