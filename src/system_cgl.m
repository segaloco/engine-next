#define GL_SILENCE_DEPRECATION
#import <AppKit/AppKit.h>

#include <OpenGL/gl.h>

#include "system.h"

@interface CglDelegate : NSObject<NSWindowDelegate>
@end

@implementation CglDelegate
- (BOOL)windowShouldClose:(NSWindow *) window
{
    system_quit();
    return YES;
}
@end

#define DEFAULT_WIDTH   640
#define DEFAULT_HEIGHT  480

static char _running;
static char _fullscreen;
static system_key_t _key_pressed;
static system_key_t _key_held;
static unsigned int _display_width;
static unsigned int _display_height;
static const char *_name;

static CglDelegate *_delegate;
static NSWindow *_window;
static NSOpenGLView *_view;

char system_init(void)
{
    const char fullscreen = 0;
    const unsigned int width = DEFAULT_WIDTH, height = DEFAULT_HEIGHT;
    const char *name = "OpenGL";
    
    int i;
    
    NSScreen *screen;
    NSRect window_frame;
    NSRect view_frame;
    NSWindowStyleMask style_mask;
    
    _running = 1;
    _fullscreen = fullscreen;
    _key_pressed = 0;
    _key_held = 0;
    _name = name;
    
    @autoreleasepool {
        [NSApplication sharedApplication];
        
        _delegate = [CglDelegate alloc];
        style_mask = NSWindowStyleMaskClosable | NSWindowStyleMaskTitled | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable | NSWindowStyleMaskFullSizeContentView;
        if (_fullscreen) {
            window_frame = [[NSScreen mainScreen] frame];
        } else {
            window_frame = NSMakeRect(0, 0, width, height);
        }
        _display_width = window_frame.size.width;
        _display_height = window_frame.size.height;
        _window = [[NSWindow alloc]     initWithContentRect: window_frame
                                                  styleMask: style_mask
                                                    backing: NSBackingStoreBuffered
                                                      defer: NO];
        [_window autorelease];
        [_window setTitle: [NSString stringWithCString:_name encoding:NSASCIIStringEncoding]];
        [_window setDelegate: _delegate];
        view_frame = [[_window contentView] frame];
        
        _view = [[NSOpenGLView alloc] initWithFrame: view_frame
                                        pixelFormat: [NSOpenGLView defaultPixelFormat]];
        [_view autorelease];
        
        [[_window contentView] addSubview:_view];
        
        [[_view openGLContext] makeCurrentContext];
        [_window makeKeyAndOrderFront:NSApp];
    }
    
    return 0;
}

void system_step(void)
{
    NSEvent *event;
    
    glFlush();
    [[_view openGLContext] flushBuffer];
    [[_view openGLContext] update];
    
    _key_pressed = 0;
    
    while ((event = [NSApp  nextEventMatchingMask: NSEventMaskAny
                                        untilDate: nil
                                           inMode: NSDefaultRunLoopMode
                                          dequeue: YES]) != nil)
    {
        switch ([event type]) {
            case NSEventTypeKeyDown:
                break;
            case NSEventTypeKeyUp:
                break;
            default:
                [NSApp sendEvent: event];
                break;
        }
        
    }
}

void system_free(void)
{
}

char system_running(void)
{
    return _running;
}
void system_display_props(unsigned int *width, unsigned int *height)
{
    *width = _display_width;
    *height = _display_height;
}

void system_get_key_state(system_key_t *pressed, system_key_t *held)
{
    *pressed = _key_pressed;
    *held = _key_held;
}

void system_toggle_fullscreen(void)
{
    if (_fullscreen) {
        _display_width = DEFAULT_WIDTH;
        _display_height = DEFAULT_HEIGHT;
    } else {
        NSScreen *screen;
        NSRect frame;
        
        screen = [NSScreen mainScreen];
        frame = [screen frame];
        _display_width = frame.size.width;
        _display_height = frame.size.height;
    }

    [_window toggleFullScreen: nil];
    [_window makeKeyAndOrderFront:NSApp];
    
    _fullscreen = !_fullscreen;
}

void system_quit(void)
{
    _running = 0;
}
