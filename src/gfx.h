#ifndef GFX_H
#define GFX_H

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

typedef struct gfx_texture_create {
    char *name;
    float *buffer;
    unsigned int width;
    unsigned int height;
} gfx_texture_create_t;

typedef struct gfx_entry_create {
    float *vertices;
    unsigned int vertex_count;
    float *colors;
    unsigned int color_count;
    float *normals;
    unsigned int normal_count;
    float *texcoords;
    unsigned int texcoord_count;
    unsigned int *indices;
    unsigned int indices_count;
} gfx_entry_create_t;

extern char gfx_init(void);
extern void gfx_free(void);
extern void gfx_draw(void);

extern int gfx_texture_create(gfx_texture_create_t *info);
extern char gfx_is_texture_in_cache(const char *name);
extern void gfx_texture_free(const char *name);

extern int gfx_entry_create(gfx_entry_create_t *info);
extern int gfx_entry_dup(unsigned int entry_id);
extern void gfx_entry_position(unsigned int id, float x, float y, float z);
extern void gfx_entry_rotation(unsigned int id, float rotation_horizontal, float rotation_vertical);
extern void gfx_entry_scale(unsigned int id, float x, float y, float z);
extern void gfx_entry_set_texture(unsigned int id, int texture_id);
extern void gfx_entry_set_visibility(unsigned int id, char is_visible);

extern void gfx_resize(void);

extern void gfx_set_camera_position(float camera_pos_x, float camera_pos_y, float camera_pos_z);
extern void gfx_set_camera_rotation(float camera_rotation_horizontal, float camera_rotation_vertical);
extern void gfx_set_camera_vert_axis(float x, float y, float z);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif  /* GFX_H */
