#include "util.h"
#include "gfx.h"
#include "mesh.h"

#include "node.h"

static void _node_initpos(node_t *this);
static void _node_initrotate(node_t *this);
static void _node_initscale(node_t *this);

void node_init(node_t *this)
{
    _node_initpos(this);
    _node_initrotate(this);
    _node_initscale(this);

    this->is_active = 1;
    this->mesh_copy = 0;
    this->texture_copy = 0;
}

void node_free(node_t *this)
{
    if (!this->mesh_copy)
        mesh_free(&this->mesh);
}

char node_load_mesh_obj(node_t *this, const char *path)
{
    char *buffer;

    buffer = util_file_buffer(path, 0);
    if (mesh_obj_create(&this->mesh, buffer) != 0)
        return 1;

    util_file_buffer_free(buffer);
    if (mesh_gfx_insert(&this->mesh) != 0)
        return 1;

    return 0;
}

char node_copy_mesh_obj(node_t *this, node_t *parent)
{
    if (parent->mesh.entry_id != -1) {
        if ((this->mesh.entry_id = gfx_entry_dup(parent->mesh.entry_id)) == -1)
            return 1;
        this->mesh_copy = 1;
    }
    if (parent->mesh.texture_id != -1) {
        if ((this->mesh.texture_id = parent->mesh.texture_id) == -1)
            return 1;
        this->texture_copy = 1;
    }

    return 0;
}

void node_set_pos(node_t *this, float x, float y, float z)
{
    this->x_pos = x;
    this->y_pos = y;
    this->z_pos = z;

    gfx_entry_position(this->mesh.entry_id, this->x_pos, this->y_pos, this->z_pos);
}

void node_set_rot(node_t *this, float rotation_horizontal, float rotation_vertical)
{
    this->rotation_horizontal = util_fmod(rotation_horizontal, 360.0f);
    this->rotation_vertical = util_fmod(rotation_vertical, 360.0f);

    gfx_entry_rotation(this->mesh.entry_id, this->rotation_horizontal, this->rotation_vertical);
}

void node_set_scale(node_t *this, float x, float y, float z)
{
    this->x_scale = x;
    this->y_scale = y;
    this->z_scale = z;

    gfx_entry_scale(this->mesh.entry_id, this->x_scale, this->y_scale, this->z_scale);
}

void node_set_visibility(node_t *this, char is_visible)
{
    if (this->mesh.entry_id != -1)
        gfx_entry_set_visibility(this->mesh.entry_id, is_visible);
}

static void _node_initpos(node_t *this)
{
    this->x_pos = 0.0f;
    this->y_pos = 0.0f;
    this->z_pos = 0.0f;
}

static void _node_initrotate(node_t *this)
{
    this->rotation_horizontal = 0.0f;
    this->rotation_vertical = 0.0f;
}

static void _node_initscale(node_t *this)
{
    this->x_scale = 1.0f;
    this->y_scale = 1.0f;
    this->z_scale = 1.0f;
}
