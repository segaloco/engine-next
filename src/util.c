#include <limits.h>
#include <math.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"

static unsigned short _parse_le_ushort(unsigned char value[2]);
static unsigned int _parse_le_uint(unsigned char value[4]);

float util_fabs(float x)
{
    return (float) fabs((double) x);
}

float util_pow(float x, float y)
{
    return (float) pow((double) x, (double) y);
}

float util_sqrt(float x)
{
    return (float) sqrt((double) x);
}

float util_sin(float x)
{
    return (float) sin((double) x);
}

float util_cos(float x)
{
    return (float) cos((double) x);
}

float util_tan(float x)
{
    return (float) tan((double) x);
}

float util_atan2(float y, float x)
{
    return (float) atan2((double) y, (double) x);
}

float util_fmod(float numer, float denom)
{
    return (float) fmod((double) numer, (double) denom);
}

int util_sscanf(const char *s, const char *format, ...)
{
    va_list argptr;
    int retval;

    va_start(argptr, format);
    retval = vsscanf(s, format, argptr);
    va_end(argptr);

    return retval;
}

float util_strtof(const char *nptr, char **endptr)
{
    return (float) strtod(nptr, endptr);
}

int util_strcmp(const char *s1, const char *s2)
{
    return strcmp(s1, s2);
}

int util_strncmp(const char *s1, const char *s2, unsigned int n)
{
    return strncmp(s1, s2, (size_t) n);
}

void *util_memcpy(void *dest, const void *src, unsigned int n)
{
    return memcpy(dest, src, (size_t) n);
}

char *util_strdup(const char *s)
{
    char *buffer = 0;

    buffer = malloc(strlen(s) + 1);
    strcpy(buffer, s);

    return buffer;
}

void util_strdup_free(char *s)
{
    free(s);
}

float util_rtod(float radians)
{
    return (radians / UTIL_PI) * 180.0f;
}

float util_dtor(float degrees)
{
    return (degrees / 180.0f) * UTIL_PI;
}

float util_lineangle2(float x1, float y1, float x2, float y2)
{
    return util_rtod(util_atan2(y1 - y2, x1 - x2)) - 90.0f;
}

void *util_file_buffer(const char *filename, int *size)
{
    FILE *stream;
    size_t nmemb;
    char *buffer;

    if ((stream = fopen(filename, "rb")) == NULL) {
        if (size)
            *size = 0;
        return 0;
    }
    fseek(stream, 0, SEEK_END);
    nmemb = (size_t) ftell(stream);
    fseek(stream, 0, SEEK_SET);

    buffer = malloc(nmemb + 1);
    (void) fread(buffer, 1, nmemb, stream);
    fclose(stream);

    buffer[nmemb] = '\0';

    if (size)
        *size = nmemb;
    return buffer;
}

void util_file_buffer_free(void *buffer)
{
    if (buffer != NULL)
        free(buffer);
}



float *util_bitmap_to_rgb_float(const char *path, unsigned int *width, unsigned int *height)
{
    unsigned char *file_buffer;
    float *buffer;

    *width = 0;
    *height = 0;
    buffer = NULL;

    file_buffer = util_file_buffer(path, NULL);

    if (!memcmp(file_buffer, "BM", 2)) {
        int i;

        unsigned int size, offset;
        unsigned int info_header_size;
        unsigned int info_width, info_height;
        unsigned int info_bpp;
        unsigned int bytes_per_pix;
        unsigned int pixel_byte_count;
        unsigned int buffer_pos;

        size = _parse_le_uint(&file_buffer[2]);
        offset = _parse_le_uint(&file_buffer[0xA]);

        info_header_size = _parse_le_uint(&file_buffer[0xE]);
        if (info_header_size = 0x7C) {
            info_width = _parse_le_uint(&file_buffer[0x12]);
            info_height = _parse_le_uint(&file_buffer[0x16]);
            info_bpp = _parse_le_ushort(&file_buffer[0x1C]);
        } else {
            util_file_buffer_free(file_buffer);
            return buffer;
        }

        bytes_per_pix = info_bpp / 8;
        if (bytes_per_pix != 3) {
            util_file_buffer_free(file_buffer);
            return buffer;
        }

        *width = info_width;
        *height = info_height;
        pixel_byte_count = bytes_per_pix * *width * *height;
        buffer = malloc(pixel_byte_count * sizeof (float));
        buffer_pos = 0;

        for (i = offset; i < size; i += bytes_per_pix) {
            unsigned char g;
            unsigned char b;
            unsigned char r;

            b = file_buffer[i];
            g = file_buffer[i+1];
            r = file_buffer[i+2];

            buffer[buffer_pos++] = (float) r / (float) UCHAR_MAX;
            buffer[buffer_pos++] = (float) g / (float) UCHAR_MAX;
            buffer[buffer_pos++] = (float) b / (float) UCHAR_MAX;

            if (buffer_pos >= pixel_byte_count)
                break;
        }
    }

    util_file_buffer_free(file_buffer);

    return buffer;
}

void util_bitmap_to_rgb_float_free(float *buffer)
{
    if (buffer != NULL)
        free(buffer);
}

void *util_copy_array(void *array, unsigned int len)
{
    void *copy;
    size_t size;

    size = (size_t) len;
    copy = malloc(size);
    memcpy(copy, array, size);

    return copy;
}

void util_copy_array_free(void *buffer)
{
    free(buffer);
}

char **util_buffer_to_lines(char *buffer, unsigned int *line_count)
{
    char **array;
    unsigned int count;
    char *token;

    array = NULL;
    count = 0;
    token = strtok(buffer, "\n");
    do {
        size_t size;

        array = realloc(array, (count + 1) * sizeof (*array));
        size = strlen(token);
        array[count] = malloc(size + 1);
        strcpy(array[count], token);
        
        count++;
    } while ((token = strtok(NULL, "\n")) != NULL);

    *line_count = count;
    return array;
}

void util_buffer_to_lines_free(char **buffer, unsigned int line_count)
{
    int i;

    for (i = 0; i < line_count; i++)
        free(buffer[i]);

    free(buffer);
}


static unsigned short _parse_le_ushort(unsigned char value[2])
{
    return value[0] | (value[1] << 8);
}

static unsigned int _parse_le_uint(unsigned char value[4])
{
    return value[0] | (value[1] << 8) | (value[2] << 16) | (value[3] << 24);
}
