#include "system.h"
#include "gfx.h"
#include "engine.h"

int main(int argc, char *argv[])
{
    if (system_init() == 1)
        return 1;
    if (gfx_init() == 1)
        return 1;
    if (engine_init() == 1)
        return 1;

    while (system_running()) {
        system_step();
        engine_step();
    }

    engine_free();
    gfx_free();
    system_free();

    return 0;
}
