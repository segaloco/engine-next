/*
 * util - various utility functions
 *
 * many of these simply wrap ANSI or provide additions
 *
 * this is to provide an interface for porting to non-ANSI platforms
 */
#ifndef UTIL_H
#define UTIL_H

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

#define UTIL_PI 3.14159f

extern float util_fabs(float x);
extern float util_pow(float x, float y);
extern float util_sqrt(float x);
extern float util_sin(float x);
extern float util_cos(float x);
extern float util_tan(float x);
extern float util_atan2(float y, float x);
extern float util_fmod(float numer, float denom);

extern int util_sscanf(const char *s, const char *format, ...);

extern float util_strtof(const char *nptr, char **endptr);
extern int util_strcmp(const char *s1, const char *s2);
extern int util_strncmp(const char *s1, const char *s2, unsigned int n);
extern void *util_memcpy(void *dest, const void *src, unsigned int n);

extern char *util_strdup(const char *s);
extern void util_strdup_free(char *s);

extern float util_rtod(float radians);
extern float util_dtor(float degrees);

extern float util_lineangle2(float x1, float y1, float x2, float y2);

extern void *util_file_buffer(const char *path, int *size);
extern void util_file_buffer_free(void *buffer);

extern float *util_bitmap_to_rgb_float(const char *path, unsigned int *width, unsigned int *height);
extern void util_bitmap_to_rgb_float_free(float *buffer);

extern void *util_copy_array(void *array, unsigned int len);
extern void util_copy_array_free(void *buffer);

extern char **util_buffer_to_lines(char *buffer, unsigned int *line_count);
extern void util_buffer_to_lines_free(char **buffer, unsigned int line_count);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif  /* UTIL_H */
