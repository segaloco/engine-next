#include <Windows.h>

#include "system.h"

char system_init(void)
{
    return 0;
}

void system_step(void)
{}

void system_free(void)
{}

char system_running(void)
{
    return 1;
}

void system_display_props(unsigned int *width, unsigned int *height)
{}

void system_get_key_state(system_key_t *pressed, system_key_t *held)
{}

void system_toggle_fullscreen(void)
{}

void system_quit(void)
{}
