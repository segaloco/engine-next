#ifndef ENGINE_H
#define ENGINE_H

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

extern char engine_init(void);
extern void engine_free(void);
extern void engine_step(void);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif  /* ENGINE_H */