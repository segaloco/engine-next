#ifndef SYSTEM_H
#define SYSTEM_H

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

typedef enum system_key {
    SYSTEM_KEY_A =  1<<0,
    SYSTEM_KEY_B =  1<<1,
    SYSTEM_KEY_C =  1<<2,
    SYSTEM_KEY_D =  1<<3,
    SYSTEM_KEY_E =  1<<4,
    SYSTEM_KEY_F =  1<<5,
    SYSTEM_KEY_G =  1<<6,
    SYSTEM_KEY_H =  1<<7,
    SYSTEM_KEY_I =  1<<8,
    SYSTEM_KEY_J =  1<<9,
    SYSTEM_KEY_K =  1<<10,
    SYSTEM_KEY_L =  1<<11,
    SYSTEM_KEY_M =  1<<12,
    SYSTEM_KEY_N =  1<<13,
    SYSTEM_KEY_O =  1<<14,
    SYSTEM_KEY_P =  1<<15,
    SYSTEM_KEY_Q =  1<<16,
    SYSTEM_KEY_R =  1<<17,
    SYSTEM_KEY_S =  1<<18,
    SYSTEM_KEY_T =  1<<19,
    SYSTEM_KEY_U =  1<<20,
    SYSTEM_KEY_V =  1<<21,
    SYSTEM_KEY_W =  1<<22,
    SYSTEM_KEY_X =  1<<23,
    SYSTEM_KEY_Y =  1<<24,
    SYSTEM_KEY_Z =  1<<25,
    SYSTEM_KEY_Esc = 1<<26,
    SYSTEM_KEY_UP = 1<<27,
    SYSTEM_KEY_DOWN = 1<<28
} system_key_t;

extern char system_init(void);
extern void system_step(void);
extern void system_free(void);

extern char system_running(void);
extern void system_display_props(unsigned int *width, unsigned int *height);
extern void system_get_key_state(system_key_t *pressed, system_key_t *held);

extern void system_toggle_fullscreen(void);
extern void system_quit(void);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif  /* SYSTEM_H */