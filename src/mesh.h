#ifndef MESH_H
#define MESH_H

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

typedef struct mesh {
    float *vertices;
    unsigned int vertex_count;
    float *colors;
    unsigned int color_count;
    float *normals;
    unsigned int normal_count;
    float *texcoords;
    unsigned int texcoord_count;
    unsigned int *indices;
    unsigned int indices_count;
    char *texture_name;
    int entry_id;
    int texture_id;
} mesh_t;

extern void mesh_free(mesh_t *this);
extern char mesh_obj_create(mesh_t *this, char *obj_buffer);
extern char mesh_gfx_insert(mesh_t *this);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif  /* MESH_H */
