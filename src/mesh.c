#include "util.h"
#include "gfx.h"

#include "mesh.h"

#define _BUFMAX  10000

typedef struct vec3 {
    float x, y, z;
} vec3_t;

typedef vec3_t vertex_t;
typedef vec3_t normal_t;

typedef struct texcoord {
    float u, v;
} texcoord_t;

typedef struct index {
    unsigned int v, t, n;
} index_t;

static void _obj_read_vertex(const char *line, vertex_t *vertex);
static void _obj_read_texcoord(const char *line, texcoord_t *texcoord);
static void _obj_read_normal(const char *line, normal_t *normal);
static void _obj_read_index(const char *line, index_t *buffer);

char mesh_obj_create(mesh_t *this, char *obj_buffer)
{
    unsigned int i;

    char **line_list;
    unsigned int line_count;
    char *line;

    vertex_t raw_vertices[_BUFMAX];
    unsigned int raw_vertex_count = 0;

    texcoord_t raw_texcoords[_BUFMAX];
    unsigned int raw_texcoord_count = 0;

    normal_t raw_normals[_BUFMAX];
    unsigned int raw_normal_count = 0;

    index_t raw_index_buffer[_BUFMAX];
    unsigned int raw_index_count = 0;

    float vertex_buffer[_BUFMAX*3];
    unsigned int vertex_count = 0;

    float texcoord_buffer[_BUFMAX*3];
    unsigned int texcoord_count = 0;

    float normal_buffer[_BUFMAX*3];
    unsigned int normal_count = 0;

    unsigned int index_buffer[_BUFMAX];
    unsigned int index_count = 0;

    this->texture_name = 0;

    line_list = util_buffer_to_lines(obj_buffer, &line_count);

    for (i = 0; i < line_count; i++) {
        char *line = line_list[i];

        switch (line[0]) {
        case '#':
            break;
        case 'v':
            if (line[1] == ' ') {
                _obj_read_vertex(line, &raw_vertices[raw_vertex_count++]);
            } else if (line[1] == 't') {
                _obj_read_texcoord(line, &raw_texcoords[raw_texcoord_count++]);
            } else if (line[1] == 'n') {
                _obj_read_normal(line, &raw_normals[raw_normal_count++]);
            }
            break;
        case 'f':
            _obj_read_index(line, &raw_index_buffer[raw_index_count]);
            raw_index_count += 3;
            break;
        case 'm':
            if (!util_strncmp(line, "mtllib", 6))
                this->texture_name = util_strdup(&line[7]);
            break;
        default:
            break;
        }

        if (raw_vertex_count > _BUFMAX || raw_texcoord_count > _BUFMAX || raw_normal_count > _BUFMAX || raw_index_count > _BUFMAX)
            return 1;
    }

    util_buffer_to_lines_free(line_list, line_count);

    this->colors = 0;
    this->color_count = 0;

    index_count = raw_index_count;
    for (i = 0; i < index_count; i++) {
        unsigned int vertex_index = raw_index_buffer[i].v - 1;
        unsigned int texcoord_index = raw_index_buffer[i].t - 1;
        unsigned int normal_index = raw_index_buffer[i].n - 1;

        if (raw_vertex_count) {
            vertex_buffer[vertex_count]   = raw_vertices[vertex_index].x;
            vertex_buffer[vertex_count+1] = raw_vertices[vertex_index].y;
            vertex_buffer[vertex_count+2] = raw_vertices[vertex_index].z;
            vertex_count += 3;
        }

        if (raw_texcoord_count) {
            texcoord_buffer[texcoord_count] = raw_texcoords[texcoord_index].u;
            texcoord_buffer[texcoord_count+1] = raw_texcoords[texcoord_index].v;
            texcoord_count += 2;
        }

        if (raw_normal_count) {
            normal_buffer[normal_count]   = raw_normals[normal_index].x;
            normal_buffer[normal_count+1] = raw_normals[normal_index].y;
            normal_buffer[normal_count+2] = raw_normals[normal_index].z;
            normal_count += 3;
        }

        index_buffer[i] = i;

        if (vertex_count > (_BUFMAX * 3) || texcoord_count > (_BUFMAX * 3) || normal_count > (_BUFMAX * 3))
            return 1;
    }

    if ((this->vertex_count = vertex_count))
        this->vertices = util_copy_array(vertex_buffer, vertex_count * sizeof (*vertex_buffer));

    if ((this->texcoord_count = texcoord_count))
        this->texcoords = util_copy_array(texcoord_buffer, texcoord_count * sizeof (*texcoord_buffer));

    if ((this->normal_count = normal_count))
        this->normals = util_copy_array(normal_buffer, normal_count * sizeof (*normal_buffer));

    if ((this->indices_count = index_count))
        this->indices = util_copy_array(index_buffer, index_count * sizeof (*index_buffer));

    return 0;
}

void mesh_free(mesh_t *this)
{
    int i;

    if (this->texture_name)
        util_strdup_free(this->texture_name);
}

char mesh_gfx_insert(mesh_t *this)
{
    gfx_entry_create_t create_info;

    create_info.vertices = this->vertices;
    create_info.vertex_count = this->vertex_count;
    create_info.colors = this->colors;
    create_info.color_count = this->color_count;
    create_info.normals = this->normals;
    create_info.normal_count = this->normal_count;
    create_info.texcoords = this->texcoords;
    create_info.texcoord_count = this->texcoord_count;
    create_info.indices = this->indices;
    create_info.indices_count = this->indices_count;

    if ((this->entry_id = gfx_entry_create(&create_info)) == -1) {
        return 1;
    } else {
        if (this->vertex_count)
            util_copy_array_free(this->vertices);
        if (this->texcoord_count)
            util_copy_array_free(this->texcoords);
        if (this->normal_count)
            util_copy_array_free(this->normals);
        if (this->indices_count)
            util_copy_array_free(this->indices);

        this->texture_id = -1;
        if (this->texture_name && !gfx_is_texture_in_cache(this->texture_name)) {
            gfx_texture_create_t info = { 0 };

            info.name = util_strdup(this->texture_name);
            info.buffer = util_bitmap_to_rgb_float("assets/cup.bmp", &info.width, &info.height);

            if (info.width && info.height)
                this->texture_id = gfx_texture_create(&info);

            util_strdup_free(info.name);
            util_bitmap_to_rgb_float_free(info.buffer);
        }
    }

    gfx_entry_set_texture(this->entry_id, this->texture_id);

    return 0;
}

/*
 * _obj - Wavefront .OBJ reading
 *
 * Very rudimentary obj handling, one function for each line type
 * supported.  Assumes the lines all have a type, space, then
 * the values of the type.  As diverse formats are found, their
 * line characteristics will be accounted for here.
 */

#define _OBJ_VERTEX_X       0
#define _OBJ_VERTEX_Y       1
#define _OBJ_VERTEX_Z       2
#define _OBJ_VERTEX_COUNT   3

static void _obj_read_vertex(const char *line, vertex_t *vertex)
{
    int i;
    char *linedup, *line_cursor;
    float buffer[_OBJ_VERTEX_COUNT];

    linedup = util_strdup(line);
    line_cursor = &linedup[2];

    for (i = 0; i < _OBJ_VERTEX_COUNT; i++)
        buffer[i] = util_strtof(line_cursor, &line_cursor);

    vertex->x = buffer[_OBJ_VERTEX_X];
    vertex->y = buffer[_OBJ_VERTEX_Y];
    vertex->z = buffer[_OBJ_VERTEX_Z];

    util_strdup_free(linedup);
}

#define _OBJ_NORMAL_X       0
#define _OBJ_NORMAL_Y       1
#define _OBJ_NORMAL_Z       2
#define _OBJ_NORMAL_COUNT   3

static void _obj_read_normal(const char *line, normal_t *normal)
{
    int i;
    char *linedup, *line_cursor;
    float buffer[_OBJ_NORMAL_COUNT];

    linedup = util_strdup(line);
    line_cursor = &linedup[3];

    for (i = 0; i < _OBJ_NORMAL_COUNT; i++)
        buffer[i] = util_strtof(line_cursor, &line_cursor);

    normal->x = buffer[_OBJ_NORMAL_X];
    normal->y = buffer[_OBJ_NORMAL_Y];
    normal->z = buffer[_OBJ_NORMAL_Z];

    util_strdup_free(linedup);
}

#define _OBJ_TEXCOORD_U     0
#define _OBJ_TEXCOORD_V     1
#define _OBJ_TEXCOORD_COUNT 2

static void _obj_read_texcoord(const char *line, texcoord_t *texcoord)
{
    int i;
    char *linedup, *line_cursor;
    float buffer[_OBJ_TEXCOORD_COUNT];

    linedup = util_strdup(line);
    line_cursor = &linedup[3];

    for (i = 0; i < _OBJ_TEXCOORD_COUNT; i++)
        buffer[i] = util_strtof(line_cursor, &line_cursor);

    texcoord->u = buffer[_OBJ_TEXCOORD_U];
    texcoord->v = buffer[_OBJ_TEXCOORD_V];

    util_strdup_free(linedup);
}

static void _obj_read_index(const char *line, index_t *buffer)
{
    util_sscanf(line, "f %u/%u/%u %u/%u/%u %u/%u/%u", &buffer[0].v, &buffer[0].t, &buffer[0].n, &buffer[1].v, &buffer[1].t, &buffer[1].n, &buffer[2].v, &buffer[2].t, &buffer[2].n);
}
