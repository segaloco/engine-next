CFLAGS		=	-g

LIBS		=	-lm
LIBS_GLX	=	$(LIBS) -lX11 -lGL
LIBS_CGL	=	$(LIBS) -framework Foundation -framework AppKit -framework OpenGL
LIBS_WGL	=	$(LIBS) -lGL

BIN			=	game

OBJS		=	src/main.o \
				src/util.o \
				src/engine.o \
				src/mesh.o \
				src/node.o \
				
OBJS_GL		=	src/gfx_gl.o
			
OBJS_GLX	=	$(OBJS) \
				$(OBJS_GL) \
				src/system_glx.o
				
OBJS_CGL	=	$(OBJS) \
				$(OBJS_GL) \
				src/system_cgl.o
				
OBJS_WGL	=	$(OBJS) \
				$(OBJS_GL) \
				src/system_wgl.o

glx: $(OBJS_GLX)
	$(CC) $(LDFLAGS) -o $(BIN) $(OBJS_GLX) $(LIBS_GLX)
	
cgl: $(OBJS_CGL)
	$(CC) $(LDFLAGS) -o $(BIN) $(OBJS_CGL) $(LIBS_CGL)
	
wgl: $(OBJS_WGL)
	$(CC) $(LDFLAGS) -o $(BIN).exe $(OBJS_WGL) $(LIBS_WGL)

clean:
	rm -rf $(BIN) $(OBJS_CGL) $(OBJS_GLX)
